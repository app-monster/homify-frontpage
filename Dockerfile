FROM node:12.14.0

RUN mkdir /usr/src/app -p
WORKDIR /usr/src/app

ENV PATH /usr/src/app/node_modules/.bin:$PATH

# Start application
CMD ["npm", "run", "dev"]